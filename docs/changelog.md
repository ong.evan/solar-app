## Changelog

* 02/20/2022 (3 hours, 40 mins)
    * Updated run scripts to allow debug/development in Windows Subsystem Linux (WSL)
    * Installed frontend frameworks
        * material ui 5.0
        * react 17
        * react-redux
        * redux-saga
    * Added store configuration and app actions and reducers
    * Added Planet data model
    * Added Sidebar component to contain the 'Add Planet' button, as well as the planet list
    * Added PlanetItem component to render the items in the planet list
    * Added NotificationDialog component + redux states
    * Updated initial App component contents to include store and theme provider
    * Added component AdaptiveFullHeightContainer
        * allows automatic resizing to full height of the container
    * Updated initial App component contents to use AdaptiveFullHeightContainer for the ViewerContainer and Sidebar
    * Updated viewer_container component to add viewer to the redux state, and to handle window resize for the canvas element
* 02/22/2022 (1 hour, 35 mins)
    * Added ValidatedTextField component that provides value validation
    * Added AddEditPlanetDialog component
    * Configured app saga
    * Added add and edit planet actions, reducers, and sagas
    * Updated viewer component to implement addPlanet and removePlanet function
    * Added addPlanet and removePlanet saga to call the viewer's addPlanet and removePlanet function
* 02/23/2022 (1 hour)
    * Updated viewer component to implement editPlanet function
    * Added editPlanet saga to call the viewer's editPlanet function
    * Updated ValidatedTextField regex match value for alphanumeric
    * Added threex-domevents to allow listening for onClick events on the planet objects in the viewer
* 02/24/2022 (36 mins)
    * Fixed correct usage of threex-domevents
    * Updated viewer component to include a dispatch member to allow dispatching of redux actions (for planet onClick events)
    * Updated AddEditPlanetDialog component initial values for the states
    * Added a 'three dots' icon button for the PlanetItem component so that onClick of PlanetItem itself does not overlap with the delete button
    * Updated viewer component to include the rotation animations of the planets
* 02/25/2022 (2 hours, 32 mins)
    * Updated Planet model so that it accepts uuid in constructor instead of generating it's own uuid
    * Updated viewer component's addPlanet function to return the created three.js object's uuid so that it can be used by the planet model
    * Updated addPlanetSaga to pass uuid during Planet model creation
    * Updated viewer_container component so that it renders a play/pause button
    * Updated app actions, reducers, and sagas to implement play/pause handling
    * Updated viewer component to check if animation is paused or not
* 02/27/2022 (31 mins)
    * Refactor Planet model
        * motivation is to avoid multiple 'data' definitions in both the viewer and redux store
        * now model extends three.js mesh object
        * model directly implements the creation of mesh object, and it's rotation animation
        * simplifies the add/edit/remove planets in the viewer component
        * planet list in the redux state now mirrors the children of the viewer's scene
* 02/28/2022 (1 hour, 15 mins)
    * Added ColorPicker component
    * Updated AddEditPlanetDialog component to fix initial values being shown when add dialog is opened
        * Previously it shows the previous values of the text fields, now it shows default values when adding new
* 03/01/2022 (2 hours)
    * Code Cleanups
    * Added readme and changelog
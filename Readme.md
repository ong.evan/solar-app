# Solar-App Challenge

## Name
Solar Application Challenge

## Description
A simple Node.js, react.js, three.js, which implements a system that can model a simple solar system
Users can add/edit/delete planets and play/pause the model animation

## Installation

1. Install Node.js using appropriate package manager (apt-get, brew, or winget)
2. Install git
3. Clone repository: `git clone https://gitlab.com/ong.evan/solar-app.git`
4. Execute the following command in the terminal / powershell
    * npm install
5. Execute the following command to run the application
    * npm run start

## Usage
* UI displays a sidebar with 'Add Planet' button and a main viewer to its right
* Viewer contains play/pause button which plays/pauses the viewer animation
    * User can use mouse controls to zoom in/out, reposition, and change camera of viewer
* When 'Add Planet' button is clicked, Add Planet dialog is shown
    * User can input the planet name, distance from sun, size, rotation, and color
* When submit is clicked in the Add Planet Dialog, 
    * A new 'planet' is added to the viewer with the entered properties
    * A new planet item is listed below the 'Add Planet' button
* Users can click on the planet in the viewer or the 'three dot' option in the planet list to open the Edit Planet dialog
* Users can also delete existing planets through the delete option in the planet list or the delete button in the Edit Planet Dialog

## Test

* Not available at the moment

## Further Development

* Below are the further development planned given more time
    * Unit Tests and Code coverage using JEST
    * Documentation of components with JSDocs
    * Add logic/computation of orbit speed of planets

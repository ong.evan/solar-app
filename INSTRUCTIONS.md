# Reactor Solar App Challenge

The objective of this challenge is to implement a small web application called "Solar App" for editing and viewing a solar system with planets rotating around a sun.

Please complete the attached project template to implement the specifications.

## Template

You can run this template project locally with the following commands:

```shell
npm install
npm run start
```

The application will then run locally on `http://localhost:3000/` and present the following interface:

![Screenshot](./docs/instructions_ui.png)

The interface is composed of the following 2 main sections:

- The control section on the left: shows a set of UI controls (buttons, text fields...) to interact with the solar system and its planets. In the template, this section is only composed of an `ADD PLANET` button not doing anything.

- The viewer section on the right: shows the solar system in 3 dimensions, user can navigate in 3D using mouse controls and select planets by clicking on them. In the templates, it shows only 1 sphere representing the sun of the solar system.

Viewer is presenting the solar system using WebGL using the [Three.js](https://threejs.org/) library.

## Specifications

In the final version, the user should be able to the following:

- Add planets to the solar system by clicking on the `ADD PLANET` button.
- Select a planet by clicking on it in the viewer section.
- Edit the properties of the currently selected planet:
    - size
    - color
    - rotation speed
    - distance to sun
- Set the solar system in motion or stop it.

In your implementation:

- You should use a reusable component framework and run in the browser (template is written in React but you can use what you are most comfortable with).
- The solar system should be rendered in 3D (template is using Three.js but you can use what you are most comfortable with).
- Improve the user interface and pay close attention to the usability of the final product.

## Hints & tips
When working on this challenge, be sure to implement your solution as if you were in a production environment, working on a product aimed to be shipped to real users and worked on by other engineers.

Some (but not all) of the things you could think about when improving upon the project:

- Proving correctness of the code and implementation
- Documentation
- Code cleanliness
- Project and code structure
- Project tooling and reproducible builds

## What to Include in Your Submission

1. The project folder and all its contents.
2. A README file in the project folder containing clear instructions on how to build, execute, and test your program.
3. A CHANGELOG file in the project folder documenting your changes to the project. Please explain why you made each change and the assumptions you took.
4. Please zip or tar your submission files (without the unnecessary artifacts) in a directory named `yourfirst.lastname` and return via email.
5. In your email response please let us know roughly how many hours you spent on this exercise.

import { Action } from 'redux';
export interface ReduxAction<T = null> extends Action {
    payload: T
}

export type Callback<ret = any> = (() => ret) | undefined;
export type Func<PARAMS extends any[] = [], RET = any> = ((...params: PARAMS) => RET) | undefined;
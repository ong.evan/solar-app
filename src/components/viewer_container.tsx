import { Box, Fab, useTheme } from '@mui/material';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import PauseIcon from '@mui/icons-material/Pause';
import React, { useEffect, useRef, useState } from 'react';
import { useAppSelector, useAppDispatch } from '../redux_saga/hooks';
import { setViewer, toggleAnimation } from '../redux_saga/App.actions';
import { RootState } from '../redux_saga/store';
import Viewer from './../models/viewer';

function ViewerContainer() {
    const canvasRef = useRef<HTMLCanvasElement>();
    const parentRef = useRef<HTMLDivElement>();
    const [width, setWidth] = useState(800);
    const [height, setHeight] = useState(600);
    const viewer = useAppSelector(state => state.SolarAppState.viewer);
    const dispatch = useAppDispatch();
    const isPlayingAnimation = useAppSelector(state => state.SolarAppState.isPlayingAnimation);
    const theme = useTheme();

    // handle resize callback
    function handleResize() {
        if (parentRef && parentRef.current) {
            setWidth(parentRef.current.offsetWidth);
            setHeight(parentRef.current.offsetHeight);
        }
    }

    // component did mount
    useEffect(() => {
        const canvas = canvasRef.current;
        if(canvas) {
            dispatch(setViewer(new Viewer(canvas, width, height, dispatch)));
        }
        // setViewer(new Viewer(canvas, width, height));
        window.addEventListener('resize', handleResize);

        // component will unmount
        return () => {
            window.removeEventListener('resize', handleResize);
            dispatch(setViewer(undefined));
        };
    }, []);
    
    // parentRef becomes not null
    useEffect(() => {
        handleResize();
    }, [parentRef]);

    useEffect(() => {
        // add handler for changing viewer width and height
        console.log(`new width and height: ${width} x ${height}`);
        viewer && viewer.resize(width, height);
    }, [width, height]);
    
    return (
        <Box sx={{ width: '100%', height: '100%' }} ref={parentRef}>
            <Fab color='primary' 
                sx={{ position: 'absolute', margin: theme.spacing(2), zIndex: 1 }}
                onClick={() => dispatch(toggleAnimation())}
            >
                {isPlayingAnimation ? <PauseIcon /> : <PlayArrowIcon />}
            </Fab>
            <Box component='canvas' ref={canvasRef}/>
        </Box>
    );
}

export default ViewerContainer;

import { Box, Button, Dialog, DialogActions, DialogContent, SxProps, TextField, Typography, useTheme } from '@mui/material';
import React, { useEffect, useState } from 'react'
import { addPlanetSaga, closeModal, editPlanetSaga, removePlanetSaga } from '../redux_saga/App.actions';

import { useAppSelector, useAppDispatch } from '../redux_saga/hooks'
import ValidatedTextField from './ValidatedTextField';
import ColorPicker from './ColorPicker';

// TODO: Add descriptive labels (i.e. Distance/size in km)

const AddEditPlanetDialog: React.FC = (props) => {
    const isOpen = useAppSelector(state => state.SolarAppState.isModalOpen);
    const mode = useAppSelector(state => state.SolarAppState.modalType);
    const editPlanet = useAppSelector(state => state.SolarAppState.editPlanet);
    const dispatch = useAppDispatch();
    const theme = useTheme();
    const [validations, setValidations] = useState({
        distance: false,
        size: false,
        color: false,
        rotationSpeed: false,
    });

    const isValid = Boolean(mode === 'add' || (mode === 'edit' && editPlanet));

    console.log(`setting states`);
    const [name, setName] = useState<string>();
    const [distance, setDistance] = useState<number>();
    const [size, setSize] = useState<number>();
    const [color, setColor] = useState<number>(0x194D33);
    const [rotationSpeed, setRotationSpeed] = useState<number>();
    
    useEffect(() => {
        let defaultName = 'New Planet';
        let defaultDist = 100;
        let defaultSize = 15;
        let defaultColor = 0x194D33;
        let defaultRotationSpeed = 0.005;
        if (mode === 'edit' && editPlanet) {
            console.log(`edit mode`)
            if (editPlanet.name !== undefined) defaultName = editPlanet.name;
            if (editPlanet.distance !== undefined) defaultDist = editPlanet.distance;
            if (editPlanet.size !== undefined) defaultSize = editPlanet.size;
            if (editPlanet.color !== undefined) defaultColor = editPlanet.color;
            if (editPlanet.rotationSpeed !== undefined) defaultRotationSpeed = editPlanet.rotationSpeed;
        }
        console.log(defaultName, defaultDist, defaultSize, defaultColor, defaultRotationSpeed);
        setName(defaultName);
        setDistance(defaultDist);
        setSize(defaultSize);
        setColor(defaultColor);
        setRotationSpeed(defaultRotationSpeed);
    }, [mode]);

    function handleClose() {
        dispatch(closeModal());
    }

    const boxStyle: SxProps = {
        display: 'flex',
        flexDirection: 'column',
    }
    
    const textFieldStyle: SxProps = {
        width: '50ch',
        margin: theme.spacing(1),
    }

    function handleColorChange(colorStr: string) {
        const colorHex = Number(colorStr.replace('#', '0x'));
        setColor(colorHex);
    }

    function renderForm() {
        return (
            <Box sx={boxStyle}>
                <ValidatedTextField 
                    variant='outlined' 
                    sx={textFieldStyle} 
                    label='Planet Name'
                    defaultValue={name}
                    onBlur={({target}) => setName(target.value)}
                />
                <ValidatedTextField 
                    variant='outlined' 
                    sx={textFieldStyle} 
                    label='Distance from Sun'
                    defaultValue={distance}
                    validation='numeric'
                    onValidationChange={(error) => setValidations({
                        ...validations,
                        distance: Boolean(error)
                    })}
                    onBlur={({target}) => setDistance(Number(target.value))}
                />
                <ValidatedTextField 
                    variant='outlined' 
                    sx={textFieldStyle} 
                    label='Size'
                    defaultValue={size}
                    validation='numeric'
                    onValidationChange={(error) => setValidations({
                        ...validations,
                        size: Boolean(error)
                    })}
                    onBlur={({target}) => setSize(Number(target.value))}
                />
                <ColorPicker 
                    label='Color'
                    sx={textFieldStyle}
                    defaultValue={`#${color.toString(16)}`}
                    onChangeComplete={handleColorChange}/>
                <ValidatedTextField 
                    variant='outlined' 
                    sx={textFieldStyle} 
                    label='Rotation Speed'
                    defaultValue={rotationSpeed}
                    validation='numeric'
                    onValidationChange={(error) => setValidations({
                        ...validations,
                        rotationSpeed: Boolean(error)
                    })}
                    onBlur={({target}) => setRotationSpeed(Number(target.value))}
                />
            </Box>
        )
    }

    function handleDelete() {
        dispatch(closeModal());
        editPlanet && dispatch(removePlanetSaga(editPlanet.uuid));
    }

    function handleAddSubmit() {
        dispatch(closeModal());
        // id is not used in add planet saga
        dispatch(addPlanetSaga({name, distance, size, color, rotationSpeed, id: ''}));
    }

    function handleEditSubmit() {
        dispatch(closeModal());
        if (editPlanet) {
            dispatch(editPlanetSaga({
                id: editPlanet.uuid,
                name, distance, color, size,
                rotationSpeed
            }));
        }
    }

    const disableSubmit:boolean = validations.distance || validations.size || validations.color || validations.rotationSpeed;

    return (
        <Dialog open={isOpen}
            maxWidth='md'
            fullWidth={false}
            onClose={handleClose}>
            <DialogContent
                dividers={true}>
                {isValid ? renderForm() : (
                    <Typography>
                        Oooops, something went wrong
                    </Typography>
                )}
            </DialogContent>
            <DialogActions>
                <Button 
                    variant='outlined' 
                    sx={{ width: '10ch' }} 
                    onClick={handleClose}>
                    Cancel
                </Button>
                { mode === 'edit' && <Button
                    variant='outlined'
                    sx={{ width: '10ch' }}
                    onClick={handleDelete}>
                    Delete
                </Button>}
                <Button 
                    variant='contained' 
                    sx={{ width: '10ch' }} 
                    disabled={disableSubmit}
                    onClick={mode === 'add' ? handleAddSubmit : handleEditSubmit}>
                    Submit
                </Button>
            </DialogActions>
        </Dialog>
    );
}

export default AddEditPlanetDialog;
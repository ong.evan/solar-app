import { Box, FormControl, IconButton, InputAdornment, InputLabel, OutlinedInput, Popover, TextFieldProps } from '@mui/material';
import React, { useState, useEffect } from 'react';
import ColorizeIcon from '@mui/icons-material/Colorize';
import CloseIcon from '@mui/icons-material/Close';
import { Func } from '../types';
import { ColorChangeHandler, SketchPicker } from 'react-color';

type ColorPickerProps = TextFieldProps & {
    onChangeComplete?: Func<[string]>
}

const ColorPicker: React.FC<ColorPickerProps> = (props) => {
    const [colorPickerAnchor, setColorPickerAnchor] = useState<null | HTMLElement>(null);
    const [value, setValue] = useState<string>(props.defaultValue as string || '#194D33')

    function toggleShowColorPicker(event: React.MouseEvent<HTMLElement>) {
        setColorPickerAnchor(colorPickerAnchor ? null : event.currentTarget);
    }

    const isShownColorPicker = Boolean(colorPickerAnchor);
    const id = isShownColorPicker ? 'color-picker' : undefined;

    const onColorChanged: ColorChangeHandler = (color) => {
        setColorPickerAnchor(null);
        setValue(color.hex);
        props.onChangeComplete && props.onChangeComplete(color.hex);
    }

    return (
        <Box>
            <FormControl sx={props.sx} variant='outlined'>
                <InputLabel htmlFor='color-picker-input'>{props.label}</InputLabel>
                <OutlinedInput
                    id='color-picker-input'
                    value={value}
                    endAdornment={
                        <InputAdornment position='end'>
                            <IconButton
                                onClick={toggleShowColorPicker}
                            >
                                { isShownColorPicker ? <CloseIcon /> : <ColorizeIcon />}
                            </IconButton>
                        </InputAdornment>
                    }
                />
            </FormControl>
            <Popover 
                id={id} 
                open={isShownColorPicker} 
                onClose={toggleShowColorPicker}
                anchorEl={colorPickerAnchor}
                anchorOrigin={{
                    vertical:'bottom',
                    horizontal:'right'
                }}>
                    <SketchPicker color={value} onChangeComplete={onColorChanged} />
            </Popover>
        </Box>
    );
}

export default ColorPicker;
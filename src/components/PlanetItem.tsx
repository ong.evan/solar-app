import React from 'react';
import { 
    IconButton, 
    ListItemButton, 
    ListItemSecondaryAction, 
    ListItemText 
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Planet from '../models/Planet';
import { useAppDispatch } from '../redux_saga/hooks';
import { openConfirmationNotification } from './NotificationDialog/redux_saga/NotificationDialog.actions';
import { removePlanetSaga, showEditModal, showEditModalSaga } from '../redux_saga/App.actions';

interface PlanetItemProps {
    data: Planet
}

const PlanetItem: React.FC<PlanetItemProps> = (props) => {
    const { data } = props;
    const dispatch = useAppDispatch();

    // TODO: On Click Item also being called when clicking delete button
    function onClickItem() {
        dispatch(showEditModalSaga(data.uuid));
    }

    function onClickDelete() {
        function onConfirmDelete() {
            dispatch(removePlanetSaga(data.uuid));
        }
        dispatch(openConfirmationNotification(
            `Are you sure you want to delete ${data.name}?`,
            onConfirmDelete, `Delete`
        ));
    }

    return (
        <ListItemButton key={data.id}>
            <ListItemText primary={data.name} />
            <ListItemSecondaryAction>
                <IconButton onClick={onClickDelete}>
                    <DeleteIcon />
                </IconButton>
                <IconButton onClick={onClickItem}>
                    <MoreVertIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItemButton>
    );
}

export default PlanetItem;
import { Callback } from '../../types';

export type NotificationType = 'info' | 'error' | 'confirmation';

interface ConfirmationAction {
    action: Callback,
    label: string
}

export interface NotificationDetails {
    type: NotificationType,
    message: string,
    confirmation?: ConfirmationAction
}
import React from 'react';
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Button,
    DialogContentText,
    Box,
    Typography,
    Theme,
    useTheme,
} from '@mui/material';
import { useAppDispatch, useAppSelector } from '../../redux_saga/hooks';
import { closeNotifcationDialog } from './redux_saga/NotificationDialog.actions';
import { SxProps } from '@mui/system';
import InfoIcon from '@mui/icons-material/Info';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import HelpIcon from '@mui/icons-material/Help';


const NotificationDialog: React.FC = () => {
    const isOpen = useAppSelector(state => state.NotificationDialogState.isOpen);
    const details = useAppSelector(state => state.NotificationDialogState.details);
    const dispatch = useAppDispatch();
    const theme: Theme = useTheme();

    const contentStyle: SxProps = {
        display: 'flex',
        width: '500',
    }

    function handleClose() {
        dispatch(closeNotifcationDialog());
    }
    function handleConfirmationAction() {
        let confirmationAction = details.confirmation?.action;
        if (confirmationAction) confirmationAction();
        dispatch(closeNotifcationDialog());
    }

    function renderActions(): Array<JSX.Element> {
        let actions: Array<JSX.Element> = [];
        if (details.type === 'confirmation') {
            actions.push(
                <Button variant='outlined' onClick={handleClose}>
                    Cancel
                </Button>
            )
        }
        if (details.type === 'confirmation' && details.confirmation) {
            actions.push(
                <Button variant='contained' onClick={handleConfirmationAction}>
                    {details.confirmation.label}
                </Button>
            );
        } else {
            actions.push(
                <Button variant='contained' onClick={handleClose}>
                    OK
                </Button>
            )
        }
        return actions;
    }

    const dialogTitleElements = {
        confirmation: [
            <HelpIcon />,
            'Please confirm'
        ],
        error: [
            <ErrorOutlineIcon />,
            'An error was encounterd'
        ],
        info: [
            <InfoIcon />,
            'Notification'
        ]
    }

    function renderDialogTitle() {
        const [icon, title] = dialogTitleElements[details.type];
        return (
            <Box sx={{display: 'flex', alignItems: 'center'}}>
                {icon}
                <Typography sx={{ marginX: theme.spacing(1) }}>{title}</Typography>
            </Box>
        );
    }

    // render
    return (
        <Dialog
            open={isOpen}
            maxWidth='sm'
            onClose={details.type === 'info' ? handleClose : undefined}
        >
            <DialogTitle>
                {renderDialogTitle()}
            </DialogTitle>
            <DialogContent dividers sx={contentStyle}>
                <DialogContentText>
                    {details.message}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                {renderActions()}
            </DialogActions>
        </Dialog>
    );
}

export default NotificationDialog;
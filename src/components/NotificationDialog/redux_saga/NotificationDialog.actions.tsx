import { Callback, ReduxAction } from "../../../types";
import { NotificationDetails } from "../NotificationDialog.types";
import { Action } from 'redux';

export enum actionTypes {
    openNotificationDialog = 'open-notification-dialog',
    closeNotifcationDialog = 'close-notification-dialog',
}

export type OpenNotificationDialogAction = ReduxAction<NotificationDetails>;

/**
 * Dispatches an action to open an information notification
 * @param message information message to display
 * @returns 
 */
export function openInfoNotification(message: string) : OpenNotificationDialogAction {
    return {
        type: actionTypes.openNotificationDialog,
        payload: {
            type: 'info',
            message,
        }
    }
}

/**
 * Dispatches an action to open an error notification. Error notifications cannot be closed by clicking outside the dialog
 * @param message Error message to display to the user
 * @returns 
 */
export function openErrorNotification(message: string) : OpenNotificationDialogAction {
    return {
        type: actionTypes.openNotificationDialog,
        payload: {
            type: 'error',
            message,
        }
    }
}

/**
 * Dispatches an action to open a confirmation. Confirmation allows passing a callback to the confirmation action
 * @param message Confirmation message to display
 * @param action callback that will be performed after user clicks the confirmation button
 * @param label label for the confirmation button (default: OK)
 * @returns 
 */
export function openConfirmationNotification(
    message: string,
    action: Callback,
    label: string = 'OK',
) : OpenNotificationDialogAction {
    return {
        type: actionTypes.openNotificationDialog,
        payload: {
            type: 'confirmation',
            message,
            confirmation: { action, label }
        }
    }
}

export function closeNotifcationDialog(): Action {
    return { type: actionTypes.closeNotifcationDialog }
}
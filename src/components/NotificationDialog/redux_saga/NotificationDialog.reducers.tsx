import { actionTypes, OpenNotificationDialogAction } from "./NotificationDialog.actions";
import { NotificationDetails } from "../NotificationDialog.types";
import { Action, Reducer } from 'redux';

export interface NotificationDialogState {
    isOpen: boolean,
    details: NotificationDetails
}

const initialState: NotificationDialogState = {
    isOpen: false,
    details: {
        type: 'error',
        message: 'An Unknown error has occured',
    }
}

export const notificationDialogState: Reducer<NotificationDialogState> = (
    state: NotificationDialogState = initialState,
    action: Action
) : NotificationDialogState => {
    switch(action.type) {
        case actionTypes.openNotificationDialog: {
            const { payload } = action as OpenNotificationDialogAction;
            if (payload) {
                return {
                    isOpen: true,
                    details: payload
                }
            }
            return state;
        }
        case actionTypes.closeNotifcationDialog: {
            return {
                ...state,
                isOpen: false,
            };
        }
        default:
            return state;
    }
}
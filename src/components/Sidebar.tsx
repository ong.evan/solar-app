import { Box, Button, IconButton, List, ListItemButton, ListItemSecondaryAction, ListItemText, SxProps } from '@mui/material';
import { useTheme } from '@mui/material';
import React from 'react';
import { showAddModal } from '../redux_saga/App.actions';
import { useAppDispatch, useAppSelector } from '../redux_saga/hooks';
import AdaptiveFulllHeightContainer from './AdaptiveFullHeightContainer';
import PlanetItem from './PlanetItem';

const Sidebar: React.FC = (props) => {
    const theme = useTheme();
    const planets = useAppSelector(state => state.SolarAppState.planets);
    const dispatch = useAppDispatch();

    function onClickAddPlanet() {
        dispatch(showAddModal());
    }

    const outerBoxStyle: SxProps = {
        padding: theme.spacing(1), 
        overflow: 'hidden', 
        display: 'flex', 
        flexDirection: 'column'
    };

    const innerBoxStyle: SxProps = { 
        width: '100%', 
        height: '100%',
        background: theme.palette.background.paper,
        overflow: 'auto',
    }

    return (
        <AdaptiveFulllHeightContainer elevation={3}>
            <Box sx={outerBoxStyle}>
                <Button variant="contained" color="primary" fullWidth onClick={onClickAddPlanet}>
                    Add Planet
                </Button>
                <Box sx={innerBoxStyle}>
                    <List>
                        {planets && planets.map(planet => (
                            <PlanetItem data={planet} />
                        ))}
                    </List>
                </Box>
            </Box>
        </AdaptiveFulllHeightContainer>
    );
}

export default Sidebar;
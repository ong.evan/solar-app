import { Paper, Theme, useTheme } from '@mui/material';
import { alpha, SxProps } from '@mui/system';
import React, { useEffect, useState } from 'react';

interface AdaptiveFulllHeightContainerProps {
    elevation?: number,
    backgroundColor?: string,
    backgroundOpacity?: number,
    sx?: SxProps,
}

const AdaptiveFulllHeightContainer: React.FC<AdaptiveFulllHeightContainerProps> = 
(props) => {
    const theme: Theme = useTheme();
    const bgColor = props.backgroundColor ?? theme.palette.background.paper;
    const bgAlpha = props.backgroundOpacity ?? 0.6;
    const [windowHeight, setWindowHeight] = useState(window.innerHeight);

    useEffect(() => {
        function handleResize() {
            setWindowHeight(window.innerHeight);
        }
        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    let paperStyle: SxProps = {
        width: '100%',
        backgroundColor: alpha(bgColor, bgAlpha),
        minHeight: '500px', // consider adding minHeight props
        height: `${windowHeight - 15}px`, 
        overflow: 'auto',
        display: 'flex',
        flexDirection: 'column',
    };
    if (props.sx) {
        paperStyle = {
            ...paperStyle,
            ...props.sx,
        }
    }
    return (
        <Paper elevation={props.elevation ?? 1} sx={paperStyle}>
            {props.children}
        </Paper>
    )
}

export default AdaptiveFulllHeightContainer;
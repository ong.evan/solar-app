import React, { useState, useEffect } from 'react';
import { TextField, TextFieldProps } from '@mui/material';
import { Func } from '../types';

type ValidatedTextFieldProps = TextFieldProps & {
    validation?: 'none' | 'alphanumeric' | 'numeric',
    onValidationChange?: Func<[string | undefined]>
}

type ChangeEventHandler = React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
type BlurEventHandler = React.FocusEventHandler<HTMLTextAreaElement | HTMLInputElement>

const ValidatedTextField: React.FC<ValidatedTextFieldProps> = (props) => {
    const [value, setValue] = useState<string>(props.defaultValue as string);
    const [error, setError] = useState<string | undefined>();

    function isValid(): boolean {
        if (props.validation && props.validation === 'alphanumeric') {
            return Boolean(value.match(/^\w+$/));
        } else if (props.validation === 'numeric') {
            return !isNaN(Number(value));
        } else {
            return true;
        }
    }

    useEffect(() => {
        const newError = !isValid() ? `Value should be ${props.validation}` : undefined;
        setError(newError);
        props.onValidationChange && props.onValidationChange(newError);
    }, [value]);
    
    const onChange: ChangeEventHandler = (event) => {
        setValue(event.target.value);
        props.onChange && props.onChange(event);
    }

    const onBlur: BlurEventHandler = (event) => {
        setValue(event.target.value);
        props.onBlur && props.onBlur(event);
    }

    const newProps = {
        ...props,
        onChange,
        onBlur,
        error: Boolean(error),
        helperText: error,
    }
    const { validation, onValidationChange, ...tfProps } = newProps;

    return (
        <TextField {...tfProps} />
    );
}

export default ValidatedTextField;
import * as THREE from 'three';
import { DoubleSide } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { AppDispatch } from '../redux_saga/store';
import { showEditModalSaga } from '../redux_saga/App.actions';
import Planet from './Planet';

const initializeDomEvents = require('threex-domevents');

let THREEx: any = {}

export default class Viewer {
    scene: THREE.Scene;
    renderer: THREE.WebGLRenderer;
    playAnimate: boolean;
    private dispatch: AppDispatch;
    private camera?: THREE.PerspectiveCamera;
    private controls?: OrbitControls;
    private domevents: any;
    private rotations: Map<string, number>;

    constructor(canvas: HTMLCanvasElement, width: number, height: number, dispatch: AppDispatch) {
        this.renderer = new THREE.WebGLRenderer({
            canvas,
            antialias: true,
        });
        this.renderer.setPixelRatio(window.devicePixelRatio);

        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0x333333);

        this.initCamera(width, height);
        this.initLighting();
        this.initContent();

        //initialize domevents
        initializeDomEvents(THREE, THREEx);
        this.domevents = new THREEx.DomEvents(this.camera, this.renderer.domElement);

        this.resize(width, height);

        this.rotations = new Map<string, number>();
        this.dispatch = dispatch;
        this.playAnimate = true;
        
        this.update();
    }

    private initCamera(width: number, height: number) {
        this.camera = new THREE.PerspectiveCamera(45, width / height, 1, 5000);
        this.camera.position.set(400, 400, 400);
        this.camera.lookAt(0, 0, 0);
        this.camera.updateProjectionMatrix();

        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    }

    private initLighting() {
        this.scene.add(new THREE.AmbientLight(0x444444));

        const lights = [];
        lights[0] = new THREE.PointLight(0xaaaaaa, 1, 0);
        lights[1] = new THREE.PointLight(0xaaaaaa, 1, 0);

        lights[0].position.set(0, 400, 0);
        lights[1].position.set(400, 800, 400);

        this.scene.add(lights[0]);
        this.scene.add(lights[1]);
    }

    private initContent() {
        const meshMaterial = new THREE.MeshPhongMaterial({color: 0xfcba03, side: DoubleSide});
        const geometry = new THREE.SphereGeometry(25, 20, 20);

        const sun = new THREE.Mesh(geometry, meshMaterial);
        this.scene.add(sun);
    }


    resize(width: number, height: number) {
        this.renderer.setSize(width, height);
        if (this.camera) {
            this.camera.aspect = width / height;
            this.camera.updateProjectionMatrix();
        }
    }

    update() {
        // TODO: add exception handler for !this.camera and !this.controls
        if (this.camera) this.renderer.render(this.scene, this.camera);

        this.controls && this.controls.update();

        requestAnimationFrame(this.update.bind(this));
        if (this.playAnimate) {
            // Animate Rotations
            const planets = this.planetList();
            planets.forEach( planet => {
                planet.rotate();
            })
        }
    }

    /**
     * addPlanet to viewer
     * @param uuid unique identifier created by Planet object
     * @param distance distance from sun in km
     * @param size radius in km
     * @param color hex code of color
     * @param rotation TODO: Add rotation animation
     */
    addPlanet(name?: string, distance?: number, size?: number, color?: number, rotationSpeed?: number): string {
        const planet = new Planet({
            name,
            distance,
            size,
            color,
            rotationSpeed
        });
        const uuid = planet.uuid;
        // add onclick callback
        this.domevents.addEventListener(planet.children[0], 'click', (event: any) => {
            this.dispatch(showEditModalSaga(uuid));
        });

        // call update to render
        this.scene.add(planet);
        this.update();
        return uuid;
    }

    /**
     * edit planet in viewer
     * @param uuid uuid of planet to edit
     * @param distance distance from sun in km
     * @param size radius in km
     * @param color hex code of color
     * @param rotation TODO: update rotation animation
     */
    editPlanet(
        uuid: string, 
        name?: string, 
        distance?: number, 
        size?: number, 
        color?: number, 
        rotationSpeed?: number
    ) {
        const planet = this.scene.children.find(child => child.uuid === uuid);
        if (planet) {
            (planet as Planet).edit({ name, distance, size, color, rotationSpeed });
        }
    }

    /**
     * remove planet in viewer
     * @param uuid uuid of planet to remove
     */
    removePlanet(uuid: string) {
        const planetParent = this.scene.children.find(child => child.uuid === uuid);
        if (planetParent !== undefined && planetParent.children.length > 0) {
            const planetObj = planetParent.children[0] as THREE.Mesh;
            this.scene.remove(planetParent);
            planetObj.geometry.dispose();
            this.rotations.delete(planetParent.uuid);
        }
        this.update();
    }

    // TODO: check if necessary
    private fitCameraToScene() {
        const offset = 1.25;
        const boundingBox = new THREE.Box3();
        boundingBox.setFromObject(this.scene);
        // const center = boundingBox.getCenter();
        // const size = boundingBox.getSize
    }

    findPlanetById(uuid: string) {
        return this.scene.children.find(child => child.uuid === uuid);
    }

    planetList(): Planet[] {
        return [...this.scene.children.filter(child => (child as Planet).isPlanet)] as Planet[];
    }
}

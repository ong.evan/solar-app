import * as three from 'three';

export type PlanetProps = {
    name?: string;
    distance?: number;
    size?: number;
    color?: number;
    rotationSpeed?: number;
    orbitSpeed?: number;
}

export default class Planet extends three.Mesh {
    name: string;
    distance: number;
    size: number;
    color: number;
    rotationSpeed: number;
    orbitSpeed: number;
    readonly isPlanet: boolean = true;

    constructor({
        name, distance, size, color, rotationSpeed, orbitSpeed
    } : PlanetProps) {
        super();

        this.name = name || 'New Planet';
        this.distance = distance || 100;
        this.size = size || 5;
        this.color = color || 0x194D33;
        this.rotationSpeed = rotationSpeed || 0.005;
        this.orbitSpeed = orbitSpeed || 0.001;

        const material = new three.MeshPhongMaterial({ color: this.color, side: three.DoubleSide, wireframe: true });
        const geometry = new three.SphereGeometry(this.size, 20, 20);
        const planet = new three.Mesh(geometry, material);
        planet.position.set(this.distance, 0, 0);
        this.add(planet);
    }

    edit({
        name, distance, size, color, rotationSpeed, orbitSpeed
    } : PlanetProps) {
        const planet = this.children[0] as three.Mesh;
        if (name !== undefined) this.name = name;
        if (distance !== undefined) {
            this.distance = distance;
            planet.position.set(distance, 0, 0);
        }
        if (size !== undefined) {
            this.size = size;
            planet.geometry.dispose();
            planet.geometry = new three.SphereGeometry(size, 20, 20);
        }
        if (color !== undefined) {
            this.color = color;
            planet.material = new three.MeshPhongMaterial({ color: this.color, side: three.DoubleSide, wireframe: true });
        }
        if (rotationSpeed !== undefined) this.rotationSpeed = rotationSpeed;
        if (orbitSpeed !== undefined) this.orbitSpeed = orbitSpeed;
    }

    rotate() {
        this.rotateY(this.orbitSpeed);
        this.children[0].rotateY(this.rotationSpeed)
    }

    destroy() {
        const planet = this.children[0] as three.Mesh;
        planet.geometry.dispose();
        this.remove(planet);
    }
}
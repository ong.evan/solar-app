import React from 'react';
import ViewerContainer from './components/viewer_container';
import { Grid } from '@mui/material';

import { configureStore } from './redux_saga/store';
import { Provider } from 'react-redux';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import AdaptiveFulllHeightContainer from './components/AdaptiveFullHeightContainer';
import Sidebar from './components/Sidebar';
import NotificationDialog from './components/NotificationDialog/NotificationDialog';
import AddEditPlanetDialog from './components/AddEditPlanetDialog';
import * as styles from './app.styles';

const { store } = configureStore();
const theme = createTheme();

function App() {
    return (
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <Grid container  sx={styles.mainContainer} spacing={1}>
                    <Grid item xs={12} md={3}>
                        <Sidebar />
                    </Grid>
                    <Grid item xs={12} md={9}>
                        <AdaptiveFulllHeightContainer elevation={3}>
                            <ViewerContainer />
                        </AdaptiveFulllHeightContainer>
                    </Grid>
                </Grid>
                <NotificationDialog />
                <AddEditPlanetDialog />
            </ThemeProvider>
        </Provider>
    );
}

export default App;

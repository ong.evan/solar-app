import { SxProps } from "@mui/material";

export const mainContainer: SxProps = {
    flexDirection: "row",
    width: '99%',
    margin: 'auto'
}
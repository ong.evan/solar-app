import { actionTypes as AppActions, PlanetAction, showEditModal, updatePlanetList } from './App.actions';
import { call, put, select, takeEvery } from '@redux-saga/core/effects';
import 'regenerator-runtime/runtime';
import { ReduxAction } from '../types';
import Planet from '../models/Planet';
import Viewer from '../models/viewer';

function* getPlanetById(id: string): Generator<any> {
    const viewer = (yield select(state => state.SolarAppState.viewer)) as Viewer;
    if (viewer) {
        return viewer.findPlanetById(id);
    }
    return undefined;
}

function* showEditModalGenerator(action: ReduxAction<string>): Generator<any> {
    const id = action.payload;
    const planet = (yield getPlanetById(id)) as Planet;
    if (planet) {
        yield put(showEditModal(planet));
    }
}

function* addPlanetSagaGenerator(action: PlanetAction): Generator<any> {
    try {
        const props = action.payload;
        const viewer = (yield select(state => state.SolarAppState.viewer)) as Viewer;
        viewer.addPlanet(
            props.name,
            props.distance,
            props.size, 
            props.color, 
            props.rotationSpeed);
        yield put(updatePlanetList(viewer.planetList()));
    } catch (e:any) {
        // TODO: add proper error handling
        console.log(`Error encountered in addPlanetSagaGenerator: ${e}`);
    }
}

function* editPlanetSagaGenerator(action: PlanetAction): Generator<any> {
    try {
        const props = action.payload;
        const viewer = (yield select(state => state.SolarAppState.viewer)) as Viewer;
        viewer.editPlanet(props.id, 
            props.name, props.distance, 
            props.size, 
            props.color, 
            props.rotationSpeed
        );
        yield put(updatePlanetList(viewer.planetList()));
    } catch (e:any) {
        // TODO: add proper error handling
        console.log(`Error encountered in editPlanetSagaGenerator: ${e}`);
    }
}

function* removePlanetSagaGenerator(action: ReduxAction<string>): Generator<any> {
    try {
        const id = action.payload;
        const viewer = (yield select(state => state.SolarAppState.viewer)) as Viewer;
        viewer.removePlanet(id);
        yield put(updatePlanetList(viewer.planetList()));
    } catch (e:any) {
        // TODO: add proper error handling
        console.log(`Error encountered in removePlanetSagaGenerator: ${e}`);
    }
}

function* toggleAnimationGenerator() : Generator<any> {
    try {
        const isPlayingAnimation = (yield select(state => state.SolarAppState.isPlayingAnimation)) as boolean;
        const viewer = (yield select(state => state.SolarAppState.viewer)) as Viewer;
        viewer.playAnimate = isPlayingAnimation;
    } catch (e:any) {
        // TODO: add proper error handling
        console.log(`Error encountered in toggleAnimationGenerator: ${e}`);
    }
}

export function* AppSaga() {
    yield takeEvery(AppActions.showEditModalSaga, showEditModalGenerator);
    yield takeEvery(AppActions.addPlanetSaga, addPlanetSagaGenerator);
    yield takeEvery(AppActions.editPlanetSaga, editPlanetSagaGenerator);
    yield takeEvery(AppActions.removePlanetSaga, removePlanetSagaGenerator);
    yield takeEvery(AppActions.toggleAnimation, toggleAnimationGenerator);
}
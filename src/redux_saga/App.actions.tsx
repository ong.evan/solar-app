import { Action } from 'redux';
import Planet from '../models/Planet';
import { ReduxAction } from '../types';
import Viewer from '../models/viewer';

export enum actionTypes {
    showAddModal = 'show-add-modal',
    showEditModal = 'show-edit-modal',
    showEditModalSaga = 'show-edit-modal-saga',
    closeModal = 'close-modal',
    setViewer = 'set-viewer',
    addPlanetSaga = 'add-planet-saga',
    editPlanetSaga = 'edit-planet-saga',
    removePlanetSaga = 'remove-planet-saga',
    toggleAnimation = 'toggle-animation',
    updatePlanetList = 'update-planet-list',
}

export function showAddModal(): Action {
    return {
        type: actionTypes.showAddModal
    }
}

export function showEditModalSaga(id: string): ReduxAction<string> {
    return {
        type: actionTypes.showEditModalSaga,
        payload: id,
    }
}
export function showEditModal(planet: Planet): ReduxAction<Planet> {
    return {
        type: actionTypes.showEditModal,
        payload: planet,
    }
}

export function closeModal(): Action {
    return {
        type: actionTypes.closeModal
    }
}

export function setViewer(viewer: Viewer | undefined): ReduxAction<Viewer | undefined> {
    return {
        type: actionTypes.setViewer,
        payload: viewer,
    }
}

type PlanetProps = {
    id: string,
    name?: string;
    distance?: number;
    size?: number;
    color?: number;
    rotationSpeed?: number;
}

export type PlanetAction = ReduxAction<PlanetProps>;

export function addPlanetSaga(props: PlanetProps): ReduxAction<PlanetProps> {
    return {
        type: actionTypes.addPlanetSaga,
        payload: props,
    }
}

export function editPlanetSaga(props: PlanetProps): PlanetAction {
    return {
        type: actionTypes.editPlanetSaga,
        payload: props,
    }
}

export function removePlanetSaga(planetId: string): ReduxAction<string> {
    return {
        type: actionTypes.removePlanetSaga,
        payload: planetId,
    }
}

export function toggleAnimation(): Action {
    return {
        type: actionTypes.toggleAnimation,
    }
}

export function updatePlanetList(planets: Planet[]) : ReduxAction<Planet[]> {
    return {
        type: actionTypes.updatePlanetList,
        payload: planets,
    }
}
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import createSagaMiddleware from "@redux-saga/core";

// import reducers
import { solarAppState as SolarAppState } from './App.reducers';
import { notificationDialogState as NotificationDialogState } from '../components/NotificationDialog/redux_saga/NotificationDialog.reducers';

// import middleware?

// import sagas
import { AppSaga } from './App.sagas';

const reducers = {
    SolarAppState,
    NotificationDialogState,
}

const rootReducer = combineReducers(reducers);
const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = (window && (window as any).__REDUX_DEVTOOLS_EXTENSION__) || compose;
const middleware: any = [sagaMiddleware];

const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(...middleware),
        composeEnhancers()
    )
);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export function configureStore() {
    // run sagas
    sagaMiddleware.run(AppSaga);
    return { store }
}
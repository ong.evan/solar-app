import { actionTypes } from "./App.actions";
import { Action, Reducer } from 'redux';
import Viewer from "../models/viewer";
import { ReduxAction } from "../types";
import Planet from "../models/Planet";

export interface SolarAppState {
    isModalOpen: boolean,
    modalType?: 'add' | 'edit',
    editPlanet?: Planet,
    planets: Planet[],
    viewer?: Viewer,
    isPlayingAnimation: boolean,
}

const initialState: SolarAppState = {
    isModalOpen: false,
    viewer: undefined,
    isPlayingAnimation: true,
    planets: [],
}

export const solarAppState: Reducer<SolarAppState> = (
    state: SolarAppState = initialState,
    action: Action,
) => {
    switch(action.type) {
        case actionTypes.showAddModal: {
            return {
                ...state,
                isModalOpen: true,
                modalType: 'add'
            }
        }
        case actionTypes.showEditModal: {
            const { payload } = (action as ReduxAction<Planet>)
            return {
                ...state,
                isModalOpen: true,
                modalType: 'edit',
                editPlanet: payload,
            }
        }
        case actionTypes.closeModal: {
            return {
                ...state,
                isModalOpen: false,
                modalType: undefined,
                editPlanet: undefined,
            }
        }
        case actionTypes.setViewer: {
            const viewer = (action as ReduxAction<Viewer | undefined>).payload;
            return {
                ...state,
                viewer
            }
        }
        case actionTypes.toggleAnimation: {
            return {
                ...state,
                isPlayingAnimation: !state.isPlayingAnimation,
            }
        }
        case actionTypes.updatePlanetList: {
            const planets = (action as ReduxAction<Planet[]>).payload;
            return {
                ...state,
                planets,
            }
        }
        default:
            return state;
    }
}
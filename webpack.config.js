const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const outputDirectory = './dist';

module.exports = (env, _argv) => {
    const plugins = [
        new HtmlWebpackPlugin({
            template: './public/index.html',
            favicon: './public/favicon.ico',
        }),
    ];

    return {
        mode: 'development',
        entry: ['regenerator-runtime/runtime.js', './src/index.tsx'],
        output: {
            filename: 'bundle.js',
            path: path.join(__dirname, outputDirectory),
        },
        module: {
            rules: [
                {
                    test: /\.(js|ts|tsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                    },
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: ['style-loader', 'css-loader', 'sass-loader'],
                },
            ],
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js'],
        },
        devServer: {
            static: path.join(__dirname, outputDirectory),
            port: 3000,
            open: true,
            watchFiles: ['./src/**/*']
        },
        plugins,
    };
};
